## Why

This is just a small repo with a soultion to the problems on https://mystery.knightlab.com/

First select the crime scene report

```sql
select *
from crime_scene_report
where date = 20180115
and type = 'murder'
and city = 'SQL City';
```

Get the people that were interviewed

```sql
select
    p.id,
    p.name,
    p.license_id,
    max(p.address_number),
    p.address_street_name,
    f.event_name,
    i.transcript
from person p
join facebook_event_checkin f on f.person_id = p.id
join interview i on i.person_id = p.id
where p.name like 'Annabel %'
    or p.address_street_name = 'Northwestern Dr'
group by p.address_street_name
```

Get the murderer's name

```sql
select
    p.name
from get_fit_now_check_in  c
join get_fit_now_member m on c.membership_id = m.id
join person p on m.person_id = p.id
join drivers_license l on p.license_id = l.id
where c.check_in_date = 20180109
    and c.membership_id like '48z%'
    and l.plate_number like '%h42w%'
```

## Bonus question

Figure out who ordered the murder

```sql
select i.transcript
from interview i
join person p on i.person_id = p.id
where p.name = 'Jeremy Bowers';
```

```sql
select
    p.name,
    l.car_make,
    l.car_model,
    count(f.event_name)
from facebook_event_checkin f
join person p on f.person_id = p.id
join drivers_license l on p.license_id = l.id
where f.event_name = 'SQL Symphony Concert'
    and f.date between 20171201 and 20171231
    and l.car_make = 'Tesla'
    and l.car_model = 'Model S'
group by f.event_name
having count(f.event_name) = 3
```
